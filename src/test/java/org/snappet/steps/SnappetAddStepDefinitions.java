package org.snappet.steps;

import static org.snappet.objects.UIMap.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.snappet.base.DriverLibrary;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SnappetAddStepDefinitions {

	DriverLibrary lib = new DriverLibrary();
	WebDriver driver = DriverLibrary.driver;
	String subject = new String();

	@Before
	public void init() {
		driver = lib.getDriver();
	}
	
	@After
	public void tearDown() {
		if(driver != null) {
			driver.quit();
			driver = null;
		}
	}
	
	@Given("^I have navigated to the Snappet Teacher Dashboard login page$")
    public void i_have_navigated_to_the_snappet_teacher_dashboard_login_page() throws Throwable {
        try {
        	
        	lib.navigateToUrl();
		} catch (WebDriverException e) {
			lib.fail("Navigating to the application URL failed");
		}
    }

    @When("^I login to Snappet Teacher Dashboard with credentials$")
    public void i_login_to_snappet_teacher_dashboard_with_credentials() throws Throwable {
        if(lib.login()) {
        	System.out.println("Login to Snappet Teacher Dashboard is successful");
        }
        else {
        	lib.fail("Login failed");
        }
    }
    
    @When("^I click on \"([^\"]*)\" button in the homepage$")
    public void i_click_on_something_button_in_the_homepage(String button) throws Throwable {
        try {
			lib.waitForVisibility(activateSubjectLink);
			lib.click(activateSubjectLink);
			System.out.println("Clicked " + button + " button");
		} catch (WebDriverException e) {
			lib.fail(button + " button is not found in the homepage");
		}
    }

    @When("^I select (.+) group$")
    public void i_select_group(String group) throws Throwable {
        String selected = lib.selectGroup(group);
        if(!selected.equals(group)) {
        	lib.fail("Selected group mismatch. Expected = " + group + ", Actual = " + selected);
        }
    }

    @When("^I select the suggestion (.+)$")
    public void i_select_the_suggestion(String suggestion) throws Throwable {
        String selected = lib.selectSuggestion(suggestion);
        if(!suggestion.equals(selected)) {
        	lib.fail("Selected suggestion mismatch. Expected = " + suggestion + ", Actual  = " + selected);
        }
    }

    @When("^I enter (.+) as subject name in \"([^\"]*)\" text box$")
    public void i_enter_as_subject_name_in_something_text_box(String subjectname, String textbox) throws Throwable {
        lib.enterValue(textbox, subjectname);
    }

    @Then("^Verify homepage is displayed$")
    public void verify_homepage_is_displayed() throws Throwable {
        if(lib.verifyHomePage("Homepage")) {
        	System.out.println("Homepage is displayed");
        }
        else {
        	lib.fail("Homepage is not displayed");
        }
    }

    @Then("^Verify \"([^\"]*)\" modal is displayed$")
    public void verify_something_modal_is_displayed(String strArg1) throws Throwable {
        try {
			lib.waitForVisibility(activateSubjectModal);
			System.out.println("Activate Subject modal is displayed");
		} catch (WebDriverException e) {
			lib.fail("Activate Subject modal is not displayed");
		}
    }

    @Then("^Verify I am navigated to Step \"([^\"]*)\"$")
    public void verify_i_am_navigated_to_step_something(String step) throws Throwable {
        String active = lib.getActiveStep();
        if(active.equalsIgnoreCase(step)) {
        	System.out.println("Navigated to step " + step);
        }
        else {
        	lib.fail("Step mismatch. Expected to be in step " + step + ", but actually in step " + active);
        }
    }

    @Then("^Verify the subject (.+) is displayed in the Homepage$")
    public void verify_the_subject_is_displayed_in_the_homepage(String subjectname) throws Throwable {
        if(lib.verifySubject("Subject Name", subjectname)) {
        	System.out.println("Subect '" + subjectname + "' is displayed in the home page");
        }
        else {
        	lib.fail("Subject " + subjectname + " is not displayed after creation");
        }
    }

    @And("^Verify already activated subjects are displayed in the homepage$")
    public void verify_already_activated_subjects_are_displayed_in_the_homepage() throws Throwable {
        if(lib.verifyHomePage("Subjects")) {
        	System.out.println("Already activated subjects are displayed in the homepage");
        }
        else {
        	lib.fail("Already activated subjects are not displayed in the homepage");
        }
    }

    @And("^Verify \"([^\"]*)\" button is displayed in the homepage$")
    public void verify_something_button_is_displayed_in_the_homepage(String button) throws Throwable {
    	if(lib.verifyHomePage(button)) {
        	System.out.println(button + " button is displayed in the homepage");
        }
        else {
        	lib.fail(button + " button is not displayed in the homepage");
        }
    }

    @And("^Verify the text \"([^\"]*)\" is displayed$")
    public void verify_the_text_something_is_displayed(String text) throws Throwable {
    	String heading = lib.getModalHeading();
    	if(text.contains("<Subject>")) {
    		text = text.replace("<Subject>", subject);
    	}
    	if(heading.equals(text)) {
    		System.out.println("The text '" + text + "' is displayed in the modal");
    	}
    	else {
    		lib.fail("Modal text mismatch. Expected = " + text + ", Actual = " + heading);
    	}
    }

    @And("^Verify \"([^\"]*)\" drop down is displayed$")
    public void verify_something_drop_down_is_displayed(String dropdown) throws Throwable {
    	if(lib.isElementPresent(currentSelectedSubjectName)) {
			System.out.println(dropdown + " drop down is displayed");
		}
		else {
			lib.fail(dropdown + " drop down is not displayed");
		}
    }

    @And("^Verify Step \"([^\"]*)\" is active$")
    public void verify_step_something_is_active(String step) throws Throwable {
    	String activeStep = lib.getActiveStep();
    	if(activeStep.equals(step)) {
    		System.out.println(step + " step is active");
    	}
    	else {
    		lib.fail(step + " step is not active. Active step is " + activeStep);
    	}
    }

    @And("^I click on \"([^\"]*)\" button$")
    public void i_click_on_something_button(String button) throws Throwable {
        try {
        	if(button.equalsIgnoreCase("Next")) {
    			lib.click(nextButton);
        	}
        	else if(button.equalsIgnoreCase("Activate Subject")) {
        		lib.click(activateSubjectButton);
        	}
			
			System.out.println("Clicked " + button + " button");
		} catch (WebDriverException e) {
			e.printStackTrace();
			lib.fail(button + " button is not found");
		}
    }

    @And("^Verify \"([^\"]*)\" link is displayed$")
    public void verify_something_link_is_displayed(String link) throws Throwable {
    	try {
    		if(link.equalsIgnoreCase("Edit")) {
    			lib.waitForVisibility(editLink);
    		}
    		else if(link.equalsIgnoreCase("Other/more options")) {
    			lib.waitForVisibility(otherOrMoreOptionsLink);
    		}
			System.out.println(link + " link is displayed");
		} catch (WebDriverException e) {
			e.printStackTrace();
			lib.fail(link + " link is not displayed");
		}
    }

    @And("^Verify \"([^\"]*)\" slider is displayed$")
    public void verify_something_slider_is_displayed(String slider) throws Throwable {
    	try {
			lib.waitForVisibility(groupSlider);
			System.out.println(slider + " Slider is displayed");
		} catch (WebDriverException e) {
			e.printStackTrace();
			lib.fail(slider + " Slider is not displayed");
		}
    }

    @And("^Verify suggestions are displayed$")
    public void verify_suggestions_are_displayed() throws Throwable {
        try {
			lib.waitForVisibility(suggestions);
			System.out.println("Suggestions are displayed");
		} catch (WebDriverException e) {
			e.printStackTrace();
			lib.fail("Suggestions are not displayed");
		}
    }

    @And("^Verify \"([^\"]*)\" text box is displayed$")
    public void verify_something_text_box_is_displayed(String strArg1) throws Throwable {
        
    }

    @And("^I select (.+) student from the \"([^\"]*)\" drop down$")
    public void i_select_student_from_the_something_drop_down(String student, String strArg1) throws Throwable {
        
    }

    @And("^Verify the subject is (.+)$")
    public void verify_the_subject_is(String subject) throws Throwable {
        if(lib.verifySubject("Subject", subject)) {
        	System.out.println("Subject is " + subject + " for the newly created subject");
        }
        else {
        	lib.fail("Subject is not " + subject + " for the newly created subject");;
        }
    }
    
    
    @And("^Verify \"([^\"]*)\" button is displayed$")
    public void verify_something_button_is_displayed(String button) throws Throwable {
        try {
        	if(button.equalsIgnoreCase("Next")) {
        		lib.waitForVisibility(nextButton);
        	}
        	else if(button.equalsIgnoreCase("Activate Subject")) {
        		lib.waitForVisibility(activateSubjectButton);
        	}
        	else {
        		lib.fail("Unknown button '" + button + "'");
        	}
			System.out.println(button + " button is displayed");
		} catch (WebDriverException e) {
			e.printStackTrace();
			lib.fail(button + " button is not displayed");
		}
    }
    
    @When("^I select (.+) subject from the \"([^\"]*)\" drop down$")
    public void i_select_subject_from_the_something_drop_down(String subject, String dropdown) throws Throwable {
    	this.subject = subject;
        String selected = lib.selectFromDropDown(dropdown, subject);
        if(!selected.equals(subject)) {
        	lib.fail("Selected subject mismatch. Expected = " + subject + ", Actual = " + selected);
        }
    }
}
