package org.snappet.steps;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.snappet.base.DriverLibrary;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.snappet.objects.UIMap.*;

public class SnappetEditStepDefinitions {

	DriverLibrary lib = new DriverLibrary();
	WebDriver driver = DriverLibrary.driver;
	
	@When("^I click \"([^\"]*)\" link of the subject (.+)$")
    public void i_click_something_link_of_the_subject(String link, String subject) throws Throwable {
        try {
			lib.clickSubjectEditLink(subject);
		} catch (WebDriverException e) {
			lib.fail("Clicking " + link + " failed");
		}
    }

    @When("^(.+) is entered in the \"([^\"]*)\" text box$")
    public void is_entered_in_the_something_text_box(String editsubjectname, String textbox) throws Throwable {
        try {
			lib.waitForVisibility(editSubjectCard);
			if(lib.isElementPresent(editSubjectCard, editSubjectCardSubjectNameTextBoxBy)) {
				
				WebElement element = driver.findElement(editSubjectCard).findElement(editSubjectCardSubjectNameTextBoxBy);
				element.clear();
				element.sendKeys(editsubjectname);
			}
		} catch (WebDriverException e) {
			e.printStackTrace();
			lib.fail("Failed to enter '" + editsubjectname + "' in " + textbox + " text box");;
		}
    }

    @Then("^Verify \"([^\"]*)\" card is displayed$")
    public void verify_something_card_is_displayed(String card) throws Throwable {
        try {
			lib.waitForVisibility(editSubjectCard);
		} catch (WebDriverException e) {
			lib.fail(card + " card is not displayed");
		}
    }

    @Then("^Verify a toast message is displayed$")
    public void verify_a_toast_message_is_displayed() throws Throwable {
        try {
			lib.waitForVisibility(toastMessage);
			System.out.println("Toast message is displayed");
		} catch (WebDriverException e) {
			lib.fail("Toast message is not displayed");
		}
    }

    @And("^Verify \"([^\"]*)\" link is displayed for (.+) subject$")
    public void verify_something_link_is_displayed_for_subject(String link, String subjectname) throws Throwable {
        if(lib.verifySubjectEditLink(subjectname)) {
        	System.out.println(link + " link is displayed for the subject '" + subjectname + "'");
        }
        else {
        	lib.fail(link + " link is not displayed for the subject " + subjectname);
        }
    }

    @And("^Verify (.+) is displayed in the \"([^\"]*)\" text box$")
    public void verify_is_displayed_in_the_something_text_box(String subjectname, String textbox) throws Throwable {
        try {
			lib.waitForVisibility(editSubjectCard);
			if(lib.isElementPresent(editSubjectCard, editSubjectCardSubjectNameTextBoxBy)) {
				String value = driver.findElement(editSubjectCard).findElement(editSubjectCardSubjectNameTextBoxBy)
						.getAttribute("value");
				if(value.equals(subjectname)) {
					System.out.println(subjectname + " is displayed in " + textbox + " text box");
				}
				else {
					lib.fail(textbox + " values are not matching. Expected = " + subjectname + ", Actual = " + value);
				}
			}
			else {
				lib.fail(textbox + " text box is not displayed");
			}
		} catch (WebDriverException e) {
			lib.fail("Error while retrieving value of " + textbox + " text box");
		}
    }

    @And("^Verify (.+) is selected in the \"([^\"]*)\" drop down$")
    public void verify_is_selected_in_the_something_drop_down(String group, String dropdown) throws Throwable {
        
    }

    @And("^Verify \"([^\"]*)\" button is displayed in the \"([^\"]*)\" card$")
    public void verify_something_button_is_displayed_in_the_something_card(String button, String card) throws Throwable {
       if(lib.verifyEditSubjectCardComponents(button)) {
    	   System.out.println(button + " button is displayed in the " + card + " card");
       }
       else {
    	   lib.fail(button + " button is not displayed in the " + card + " card");
       }
    }

    @And("^(.+) is selected in the \"([^\"]*)\" drop down$")
    public void change_is_selected_in_the_something_drop_down(String editgroup, String dropdown) throws Throwable {
        try {
			if(lib.isElementPresent(editSubjectCard, editSubjectCardGroupDropDownBy)) {
				WebElement grp = driver.findElement(editSubjectCard).findElement(editSubjectCardGroupDropDownBy);
				grp.click();
				lib.waitForVisibility(editSubjectGroups);
				for(WebElement g : driver.findElements(editSubjectGroups)) {
					String value = g.getText();
					if(value.equals(editgroup)) {
						g.click();
						System.out.println("Selected group " + value);
						break;
					}
				}
			}
			else {
				lib.fail(dropdown + " drop down is not found in the Edit Subject card");
			}
		} catch (WebDriverException e) {
			lib.fail("Error while selecting '" + editgroup + "' from " + dropdown + " dropdown");
		}
    }

    @And("^Click \"([^\"]*)\" button in the \"([^\"]*)\" card$")
    public void click_something_button_in_the_something_card(String button, String card) throws Throwable {
        try {
			if(lib.isElementPresent(editSubjectCard, editSubjectCardSaveButtonBy)) {
				lib.jsClick(driver.findElement(editSubjectCard).findElement(editSubjectCardSaveButtonBy));
				System.out.println("Clicked " + button + " button");
			}
			else {
				lib.fail(button  + " is not found in " + card + " card");
			}
		} catch (WebDriverException e) {
			lib.fail("Clicking " + button + " in " + card + " card is failed");
		}
    }

    @And("^Verify the message text is \"([^\"]*)\"$")
    public void verify_the_message_text_is_something(String message) throws Throwable {
        if(lib.isElementPresent(toastMessage, toastMessageTextBy)) {
        	String value = driver.findElement(toastMessage).findElement(toastMessageTextBy).getText();
        	if(value.equals(message)) {
        		System.out.println("Toast message text is '" + message + "'");
        	}
        	else {
        		lib.fail("Toast message text mismatch. Expected = " + message + ", Actual = " + value);
        	}
        }
        else {
        	System.out.println("Toast message text is not found");
        }
    }
    
    @And("^Verify (.+) is selected in the \"([^\"]*)\" drop down in the \"([^\"]*)\" card$")
    public void verify_is_selected_in_the_something_drop_down_in_the_something_card(String group, String dropdown, String strArg2) throws Throwable {
    	try {
			if(lib.isElementPresent(editSubjectCard, editSubjectCardGroupDropDownBy)) {
				String value = driver.findElement(editSubjectCard).findElement(editSubjectCardGroupDropDownBy)
						.getText().trim();
				if(value.equals(group)) {
					System.out.println(group + " is selected in " + dropdown + " dropdown");
				}
				else {
					lib.fail("Group name is not matching. Expected = " + group + ", Actual = " + value);
				}
			}
			else {
				lib.fail(dropdown + " drop down is not found in the Edit Subject card");
			}
		} catch (Exception e) {
			lib.fail("Error while retrieving the value of " + dropdown + " dropdown");
		}
    }

}
