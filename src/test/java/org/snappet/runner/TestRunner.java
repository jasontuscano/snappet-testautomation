package org.snappet.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = "classpath:feature", glue = "org.snappet.steps", 
					plugin = {"json:report.json", "html:html-report"}, 
					tags = {"@Edit"})
public class TestRunner extends AbstractTestNGCucumberTests {

}
