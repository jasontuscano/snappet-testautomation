#Author : 

@Snappet
Feature: Snappet Teacher Dashboard
  This feature is written to verify creation and editing of subjects work fine
		
  @Add
  Scenario Outline: Adding Subject
  	Given I have navigated to the Snappet Teacher Dashboard login page
    When I login to Snappet Teacher Dashboard with credentials
    Then Verify homepage is displayed
    And Verify already activated subjects are displayed in the homepage
    And Verify "Activate Subject" button is displayed in the homepage
    When I click on "Activate Subject" button in the homepage
    Then Verify "Activate Subject" modal is displayed
    And Verify the text "Which subject do you want to activate?" is displayed
    And Verify "Subject" drop down is displayed
    And Verify Step "1" is active
    And Verify "Next" button is displayed
    When I select <Subject> subject from the "Subject" drop down
    And I click on "Next" button
    Then Verify I am navigated to Step "2"
    And Verify the text "At what level does this group work?" is displayed
    And Verify "Edit" link is displayed
    And Verify "Group" slider is displayed
    And Verify "Next" button is displayed
    When I select <Group> group
    And I click on "Next" button
    Then Verify I am navigated to Step "3"
    And Verify the text "These are our suggestions for <Subject>" is displayed
    And Verify suggestions are displayed
    And Verify "Edit" link is displayed
    And Verify "Other/more options" link is displayed
    When I select the suggestion <Suggestion>
    Then Verify I am navigated to Step "4"
    And Verify the text "Finalize your subject/course for" is displayed
    And Verify "Subject Name" text box is displayed
    And Verify "Student" drop down is displayed
    And Verify "Activate Subject" button is displayed
    When I enter <SubjectName> as subject name in "Subject Name" text box
    And I click on "Activate Subject" button
    Then Verify the subject <SubjectName> is displayed in the Homepage
    And Verify the subject is <Subject>
    
   Examples: 
      | Subject  | Group | Suggestion                     | SubjectName    |
      | Rekenen  |  5    | A. Pluspunt v3, gr 5           | Test Snappet   |
      | Spelling |  4    | C. SP TA3-4 uitgebreide versie | Test Snappet 1 |
      
  @Edit
  Scenario Outline: Editing Subject
    Given I have navigated to the Snappet Teacher Dashboard login page
    When I login to Snappet Teacher Dashboard with credentials
    Then Verify homepage is displayed
    And Verify already activated subjects are displayed in the homepage
    And Verify the subject <SubjectName> is displayed in the Homepage
    And Verify the subject is <Subject>
    And Verify "Edit" link is displayed for <SubjectName> subject
    When I click "Edit" link of the subject <SubjectName>
    Then Verify "Edit Subject" card is displayed
    And Verify <SubjectName> is displayed in the "Subject Name" text box
    And Verify <Group> is selected in the "Group" drop down in the "Edit Subject" card
    And Verify "Change Students" button is displayed in the "Edit Subject" card
    And Verify "Cancel" button is displayed in the "Edit Subject" card
    And Verify "Save" button is displayed in the "Edit Subject" card
    And Verify "Remove Subject" button is displayed in the "Edit Subject" card
    When <EditSubjectName> is entered in the "Subject Name" text box
    And <EditGroup> is selected in the "Group" drop down
    And Click "Save" button in the "Edit Subject" card
    Then Verify a toast message is displayed
    And Verify the message text is "Subject changes have been saved"
    
    
   Examples: 
      | Subject | Group | SubjectName | EditSubjectName | EditGroup |
      | Spelling  |  Groep 5    | Test Snappet 1 | Test Snappet 2 | 5 |