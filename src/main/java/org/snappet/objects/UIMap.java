package org.snappet.objects;

import org.openqa.selenium.By;

public class UIMap {

	public static By usernameTextBox = By.id("UserName");
	public static By passwordField = By.id("Password");
	public static By loginButton = By.xpath("//button[@type='submit']");
	public static By forgotPasswordLink = By.xpath("//a[contains(@class,'btn-forget')]");
	
	//Homepage tabs
	public static By homeTab = By.xpath("//a[@title='Home']");
	public static By lessonsTab = By.xpath("//a[@title='Lessons']");
	public static By workingSetsTab = By.xpath("//a[@title='Working Sets']");
	public static By monitorTab = By.xpath("//a[@title='Monitor']");
	public static By reportsTab = By.xpath("//a[@title='Reports']");
	public static By searchButton = By.xpath("//search/a");
	
	//Homepage subjects
	public static By subjects = By.xpath("//div[@class='panel-card alternate']");
	public static By subjectGroupNameBy = By.xpath(".//span[contains(@class,'panel-card-heading')]/strong");
	public static By subjectSubjectNameBy = By.xpath(".//span[contains(@class,'panel-card-heading')]/small");
	public static By subjectEditButtonBy = By.xpath(".//div[@class='button-bar left']/a");
	
	//Homepage activate subject
	public static By activateSubjectLink = By.xpath("//div[@class='subject-col']//a[@class='add-first-lesson']");
	public static By activateSubjectModal = By.xpath("//div[contains(@class,'modal-content')]");
	public static By activateSubjectScreenHeading = By.xpath("//div[@class='header']//h2");
	public static By activateSubjectStepTab = By.xpath("//div[@class='wizard-step-overview']//div[contains(@class,'wizard-step') and @data-bind]");
	public static By activateSubjectStepIndexBy = By.xpath("./span[@data-bind]");
	
	public static By currentSelectedSubjectName = By.xpath("//span[contains(@id,'select2')]//span");
	public static By subjectList = By.xpath("//ul[contains(@id,'select2')]/li");
	public static By subjectListSubjectNameBy = By.xpath(".//span[@data-bind='text: name']");
	public static By nextButton = By.xpath("//a[contains(@data-bind,'Next')]");
	
	public static By groupLevels = By.xpath("//div[@class='grade-slider-ticks']/div[@class='grade-slider-tick']//div[contains(@class,'inner-grade-slider-tick')]");
	public static By groupSlider = By.xpath("//input[@type='range']");
	public static By groupLevelNumberBy = By.xpath("./div[2]");
	public static By editLink = By.xpath("//span[text()='(Edit)']");
	
	public static By suggestions = By.xpath("//div[@class='card-inner']");
	public static By suggestionsTitleBy = By.xpath(".//span[contains(@data-bind,'html')]");
	public static By suggestionsSubTitleBy = By.xpath(".//span[@class='subtitle']");
	public static By suggestionsContentBy = By.xpath(".//div[contains(@class,'card-content')]");
	public static By otherOrMoreOptionsLink = By.xpath("//a[@class='show-more']");
	
	public static By selectedSuggestion = By.xpath("//p[@data-bind]/span[1]");
	public static By newSubjectNameTextBox = By.xpath("//input[@data-bind='value: newGroupName']");
	public static By currentStudent = By.xpath("//div[@class='subjectGroupRow']/span");
	public static By activateSubjectButton = By.xpath("//button[contains(text(),'Activate')]");

	//Edit Subject
	public static By editSubjectCard = By.xpath("//div[@class='panel-card secondary']");
	public static By editSubjectCardSubjectNameTextBoxBy =By.xpath(".//input[@data-bind='value: subjectGroupName']");
	public static By editSubjectCardGroupDropDownBy = By.xpath(".//span[@role='combobox']");
	public static By editSubjectCardGroupListBy = By.xpath(".//ul[@id='select2-ejvv-results']");
	public static By editSubjectCardGroupNamesBy = By.xpath(".//ul[@id='select2-ejvv-results']/li");
	public static By editSubjectCardChangeStudentsButtonBy = By.xpath(".//button[contains(@data-bind,'ChangePupils')]");
	public static By editSubjectCardCancelButtonBy = By.xpath(".//button[contains(@data-bind,'cancel')]");
	public static By editSubjectCardSaveButtonBy = By.xpath(".//button[contains(@data-bind,'saveSubjectGroup')]");
	public static By editSubjectCardRemoveSubjectButtonBy = By.xpath("//button[contains(@data-bind,'removeSubjectGroup')]");
	public static By editSubjectGroups = By.xpath("//ul[@class='select2-results__options']/li");
	
	public static By toastMessage = By.xpath("//div[contains(@class,'pnotify-snappet')]");
	public static By toastMessageCloseButtonBy = By.xpath("./div[@class='ui-pnotify-closer']/span");
	public static By toastMessageTextBy = By.xpath(".//div[@class='ui-pnotify-text']");
	
	public static By loadingIcon = By.xpath("//div[@class='loader loader-clone']//div[@class='gears']");
	
	
}
