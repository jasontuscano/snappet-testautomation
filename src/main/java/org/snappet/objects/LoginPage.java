package org.snappet.objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.snappet.base.DriverLibrary;
import org.snappet.base.Settings;

public class LoginPage extends DriverLibrary{

	private By usernameTextBox = By.id("UserName");
	private By passwordField = By.id("Password");
	private By loginButton = By.xpath("//button[@type='submit']");
	
	public boolean login() {
		String username = Settings.getProperty("Username");
		String password = Settings.getProperty("Password");
		boolean loggedIn = false;
		try {
			waitForVisibility(usernameTextBox);
			driver.findElement(usernameTextBox).sendKeys(username);
			driver.findElement(passwordField).sendKeys(password);
			click(loginButton);
			waitForInvisibility(loginButton);
			loggedIn = true;
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
		return loggedIn;
	}
}
