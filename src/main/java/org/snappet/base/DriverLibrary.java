package org.snappet.base;

import static org.snappet.objects.UIMap.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class DriverLibrary {

	public static WebDriver driver;
	private String driverPath = Settings.getProperty("ChromeDriverPath");
	private int timeout = Integer.parseInt(Settings.getProperty("Timeout"));
	private String url = Settings.getProperty("URL");
	private String username = Settings.getProperty("Username");
	private String password = Settings.getProperty("Password");
	
	public DriverLibrary() {
		
	}
	
	public WebDriver getDriver() {
		System.setProperty("webdriver.chrome.driver", driverPath);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		return driver;
	}
	
	
	public void waitForVisibility(By by) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
	}
	
	public void jsClick(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		try {
			js.executeScript("arguments[0].click();", element);
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
	}
	
	public void click(By by) {
		WebElement element = driver.findElement(by);
		try {
			element.click();
		} catch (WebDriverException e) {
			jsClick(element);
		}
	}
	
	public boolean isElementPresent(By by) {
		boolean pressent = false;
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			pressent = driver.findElement(by).isDisplayed();
		} catch (WebDriverException e) {
			
		}
		driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
		return pressent;
	}
	
	public void waitForInvisibility(By by) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
		driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
	}
	
	public void waitForInvisibility(By by, int timeout) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
		driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
	}

	public boolean isElementPresent(By by, By innerBy) {
		boolean pressent = false;
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			pressent = driver.findElement(by).findElement(innerBy).isDisplayed();
		} catch (WebDriverException e) {
			
		}
		driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
		return pressent;
	}
	
	public void navigateToUrl() {
		try {
			if(driver == null) {
				driver = getDriver();
			}
			driver.get(url);
			System.out.println("Navigate to URL " + url);
			
		} catch (WebDriverException e) {
			e.printStackTrace();
			throw new WebDriverException();
		}
	}
	
	public boolean login() {
		boolean login = false;
		try {
			waitForVisibility(usernameTextBox);
			driver.findElement(usernameTextBox).sendKeys(username);
			driver.findElement(passwordField).sendKeys(password);
			try {
				waitForInvisibility(loginButton, 2);
			} catch (WebDriverException e) {
			}
			click(loginButton);
			if(isElementPresent(loginButton)) {
				jsClick(driver.findElement(loginButton));
			}
			waitForInvisibility(loginButton);
			login = true;
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
		return login;
	}
	
	public void fail(String message) {
		System.err.println(message);
		Assert.fail(message);
	}
	
	public boolean verifyHomePage(String element) {
		boolean verify = false;
		try {
			if(element.equalsIgnoreCase("Homepage")) {
				waitForVisibility(homeTab);
			}
			else if(element.equalsIgnoreCase("Activate Subject")) {
				waitForVisibility(activateSubjectLink);
			}
			else if(element.equalsIgnoreCase("Subjects")) {
				waitForVisibility(subjects);
			}
			else {
				fail("Unhandled element '" + element + "'");
			}
			verify = true;
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
		return verify;
	}
	
	public boolean verifyActivateSubjectModal(String element) {
		boolean verify = false;
		try {
			waitForVisibility(activateSubjectModal);
			verify = true;
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
		return verify;
	}
	
	public String getActiveStep() {
		String step = new String();
		boolean found = false;
		try {
			if(isElementPresent(activateSubjectStepTab)) {
				List<WebElement> steps = driver.findElements(activateSubjectStepTab);
	    		for(WebElement s : steps) {
	    			String className = s.getAttribute("class");
	    			if(className.contains("active")) {
	    				found = true;
	    				step = s.findElement(activateSubjectStepIndexBy).getText();
	    				break;
	    			}
	    		}
	    		if(!found) {
	    			fail("No active step was found");
	    		}
			}
			else {
				fail("Step is not found");
			}
			
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
		return step;
	}
	
	public String selectFromDropDown(String dropdown, String value) {
		String selected = new String();
		try {
			if(dropdown.equalsIgnoreCase("Subject")) {
				waitForVisibility(currentSelectedSubjectName);
				click(currentSelectedSubjectName);
				System.out.println("Clicked Subject name drop down");
				waitForVisibility(subjectList);
				List<WebElement> subjects = driver.findElements(subjectList);
				for(WebElement s : subjects) {
					String name = s.findElement(subjectListSubjectNameBy).getText();
					if(name.equalsIgnoreCase(value)) {
						jsClick(s);
						System.out.println("Selected Subject '" + name + "'");
						selected = name;
						return selected;
					}
				}
				fail(value + " is not found in the drop down");
			}
			else {
				fail("Unhandled dropdown '" + dropdown + "'");
			}
		} catch (WebDriverException e) {
			e.printStackTrace();
			fail("Error while selecting '" + value + "' from '" + dropdown + "' dropdown");
		}
		return selected;
	}
	
	public String getModalHeading() {
		String heading = new String();
		try {
			waitForInvisibility(loadingIcon);
	    	if(isElementPresent(activateSubjectScreenHeading)) {
				heading = driver.findElement(activateSubjectScreenHeading)
						.getText();
			}
			else {
				fail("Activate Subject Modal text is not found");
			}
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
		return heading;
	}
	
	public String selectGroup(String group) {
		String selected = new String();
		try {
			waitForVisibility(groupLevels);
			List<WebElement> groups = driver.findElements(groupLevels);
			for(WebElement g : groups) {
				String name = g.findElement(groupLevelNumberBy).getText();
				if(name.equals(group)) {
					g.click();
					System.out.println("Selected group " + group);
					selected = name;
					return selected;
				}
			}
			fail("Group " + group + " is not found");
		} catch (WebDriverException e) {
			e.printStackTrace();
			fail("Error while selecting Group");
		}
		return selected;
	}
	
	public String selectSuggestion(String suggestion) {
		String selected = new String();
		try {
			waitForVisibility(suggestions);
			boolean found = false;
			boolean moreOptions = false;
			do {
				List<WebElement> suggestionList = driver.findElements(suggestions);
				for(WebElement s : suggestionList) {
					String name = s.findElement(suggestionsTitleBy).getText();
					if(name.equals(suggestion)) {
						found = true;
						s.click();
						selected = name;
						System.out.println("Selected suggestion '" + suggestion + "'");
						break;
					}
				}
				if(!found) {
					if(moreOptions) {
						fail("Suggestion '" + suggestion + "' is not found");
						break;
					}
					waitForVisibility(otherOrMoreOptionsLink);
					click(otherOrMoreOptionsLink);
					System.out.println("Clicked Other/More Options link");
					moreOptions = true;
				}
			}
			while(!found);
		} catch (WebDriverException e) {
			e.printStackTrace();
			fail("Error while selecting suggestion");
		}
		return selected;
	}
	
	public void enterValue(String textbox, String value) {
		try {
			if(textbox.equalsIgnoreCase("Subject Name")) {
				waitForVisibility(newSubjectNameTextBox);
				WebElement subTextbox = driver.findElement(newSubjectNameTextBox);
				subTextbox.clear();
				subTextbox.sendKeys(value);
				System.out.println("Entered '" + value + "' in '" + textbox + "' text box");
			}
		} catch (WebDriverException e) {
			e.printStackTrace();
			fail("Error while entering '" + value + "' in '" + textbox + "' text box");
		}
	}
	
	public boolean verifySubject(String component, String value) {
		boolean verify = false;
		By by = null;
		try {
			if(component.equalsIgnoreCase("Subject Name")) {
				by = subjectGroupNameBy;
			}
			else if(component.equalsIgnoreCase("Subject")) {
				by = subjectSubjectNameBy;
			}
			else {
				fail("Unhandled component '" + component + "'");
			}
			waitForInvisibility(loadingIcon);
			try {
				waitForInvisibility(subjects, 2);
			} catch (WebDriverException e) {
				
			}
			waitForVisibility(subjects);
			List<WebElement> subjectList = driver.findElements(subjects);
			for(WebElement s : subjectList) {
				String text = s.findElement(by).getText();
				if(text.equals(value)) {
					scrollIntoView(s);
					verify = true;
					return verify;
				}
			}
			fail(component + " '"+ value + "' is not found in the created subject");
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
		return verify;
	}
	
	public boolean verifySubjectEditLink(String subject) {
		boolean verify = false;
		try {
			List<WebElement> subjectList = driver.findElements(subjects);
			for(WebElement s : subjectList) {
				String text = s.findElement(subjectGroupNameBy).getText();
				if(text.equals(subject)) {
					verify = s.findElement(subjectEditButtonBy).isDisplayed();
					break;
				}
			}
		} catch (WebDriverException e) {
			e.printStackTrace();
			fail("Edit link is not found for the subject '" + subject + "'");
		}
		return verify;
	}
	
	public void clickSubjectEditLink(String subject) {
		try {
			List<WebElement> subjectList = driver.findElements(subjects);
			for(WebElement s : subjectList) {
				String text = s.findElement(subjectGroupNameBy).getText();
				if(text.equals(subject)) {
					jsClick(s.findElement(subjectEditButtonBy));
					break;
				}
			}
		} catch (WebDriverException e) {
			e.printStackTrace();
			fail("Edit link is not found for the subject '" + subject + "'");
		}
	}
	
	public boolean verifyEditSubjectCardComponents(String component) {
		boolean verify = false;
		By by = null;
		try {
			if(component.equalsIgnoreCase("Change Students")) {
				by = editSubjectCardChangeStudentsButtonBy;
			}
			else if(component.equalsIgnoreCase("Cancel")) {
				by = editSubjectCardCancelButtonBy;
			}
			else if(component.equalsIgnoreCase("Save")) {
				by = editSubjectCardSaveButtonBy;
			}
			else if(component.equalsIgnoreCase("Remove Subject")) {
				by = editSubjectCardRemoveSubjectButtonBy;
			}
			else {
				fail("Unhandled component '" + component + "'");
			}
			verify = isElementPresent(editSubjectCard, by);
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
		return verify;
	}
	
	public void scrollIntoView(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		try {
			js.executeScript("arguments[0].scrollIntoView(true);", element);
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
	}
}
