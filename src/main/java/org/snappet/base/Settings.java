package org.snappet.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class Settings {

	public static String getProperty(String property) {
		String value = new String();
		try {
			File file = new File("src/main/resources/Data.properties");
			InputStream in = new FileInputStream(file);
			Properties props = new Properties();
			props.load(in);
			value = props.getProperty(property);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}
}
